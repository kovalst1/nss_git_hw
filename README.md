# Nss Git Hw

By Stanislav Kovalchuk
A simple JavaScript application for tracking expenses.


## Technologies Used

- JavaScript
- HTML
- CSS


## Description

This application allows users to add transactions to selected category and track their expenses during different periods.

## Setup

- clone repository
- open homepage.html in your browser

## Copyright

Everyone can do whatever they want with this code